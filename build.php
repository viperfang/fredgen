<?php
// Start Page Build

// Setup variables
include 'config.php';

// Search for pages & build
foreach (glob("content/pages/*") as $filename) {

    
    $filename = str_replace("content/pages/", "",$filename);
    $filename = str_replace(".php", "",$filename);

    // Setup Page Name
    $PageName = $filename;

    $PageName = ucfirst($PageName);

    // Check if index.php
    if ($filename == "index") {
        $PageName = $HomePageName;
    }

    ob_start();

    // Remove existing files
    unlink("$StorageLocation/$filename.html");

    // Get contents
    $HeaderInfo = file_get_contents("content/header-info.php");
    $Style = file_get_contents("content/style.css");
    $Menu = file_get_contents("content/menu.php");
    $Content = file_get_contents("content/pages/$filename.php");
    $Footer = file_get_contents("content/footer.php");

    // Build Layout
    include 'content/layout.php';

    $PageOutput = ob_get_contents();
    ob_end_clean(); 

    // Dump page to file
    $NewFilename = "$StorageLocation/$filename.html";
    file_put_contents($NewFilename, $PageOutput);
}

// Go back to build page.
include "index.php";

?>