# FredGen

A small FOSS static site generator. What more can be said?

# Licence
GNU General Public License v3.0

# Contribution Rules
- 4 Spaces - NOT tabs... (please)
- 80 char width soft limit (it just looks nice)
- Be excellent to each other! (it's the LAW, so deal with it.)