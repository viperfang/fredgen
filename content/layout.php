<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=$SiteName?> - <?=$PageName?></title>
    <?=$HeaderInfo?>
</head>

<style>
    <?=$Style?>

</style>

<body>

<div class="menu">
<?=$Menu?>
</div>

<div class="content">
<?=$Content?>
</div>

<div class="footer">
    <?=$Footer?>
    <br />
    Last Updated <?=$LastUpdated?> By <?=$AuthorName?>
<div>

</body>

</html>