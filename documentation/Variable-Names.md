# Variable Names
These names are used site wide and are usually defined in the 'config.php' file of the main directory. 
For example the Authors Name:
    $AuthorName = "David Collins-Cubitt"

You can incorporate these into your content pages as follows
    <div class='footer'>
        Author Name: <?=$AuthorName?>
    </div>

