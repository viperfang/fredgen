<?php

// Site Name
$SiteName = "FredGen";

// Author Name
$AuthorName = "David Collins-Cubitt";

// Homepage Name
$HomePageName = "Homepage";

// Auto generate date
$LastUpdated = Date("d-m-Y");

// Static Site Storage Location
$StorageLocation = "static-site";

?>